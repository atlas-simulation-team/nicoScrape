from artScrapeTool import *
from artAnalysisTool import *
import sys

# Create tool to access ART
myScraper = ArtScrapeTool()
if len( sys.argv ) == 1:
  myScraper.LoginPrompt()
elif len( sys.argv ) == 3:
  myScraper.Login( sys.argv[1], sys.argv[2] )
else:
  print( "Usage: artCritic.py" )
  print( "or:    artCritic.py username password" )
  exit()

# Create tool to analyse ART
myAnalysis = ArtAnalysisTool( myScraper )
myAnalysis.Summary()
#myAnalysis.Followup()
#myAnalysis.Paranoid()
