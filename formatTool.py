import datetime as dt
import sys


#Make a report section for nicos/atn
def FormatPlatinum( resultBlock, buildProjects ):

    #Group reports by project
    for project in buildProjects:

        #Sort results by severity
        errors = []
        warnings = []
        minors = []
        updating = []
        for message in resultBlock[ project ]:

            if message is None:
                continue

            state = message[1]
            if state == "ERROR":
                errors += [ message ]
            elif state == "WARNING":
                warnings += [ message ]
            elif state == "MINOR WARNING":
                minors += [ message ]
            elif state == "UPDATING":
                updating += [ message ]
            else:
                print( "Unrecognised state " + state )
                exit()

        errors.sort()
        warnings.sort()
        minors.sort()
        updating.sort()

        #Make output
        print( "\n" + project + ":" )
        if len( errors ) + len( warnings ) + len( minors ) + len( updating ) == 0:
            print( "\n  - all NA" )
        else:
            if len( errors ) > 0:
                print( "\n  - " + str( len( errors ) ) + " errors" )
                for message in errors:
                    print( "\n    " + message[0] + ": " + message[2] )
            if len( warnings ) > 0:
                print( "\n  - " + str( len( warnings ) ) + " warnings" )
                for message in warnings:
                    print( "\n    " + message[0] + ": " + message[2] )
            if len( minors ) > 0:
                print( "\n  - " + str( len( minors ) ) + " minor warnings" )
                for message in minors:
                    print( "\n    " + message[0] + ": " + message[2] )
            if len( updating ) > 0:
                print( "\n  - " + str( len( updating ) ) + " still updating" )
                for message in updating:
                    print( "\n    " + message[0] )

        print( "" )

#Make a report section for RTT
def FormatRTT( resultBlock, relNumber, yesterday, myScraper ):

    #Find out the previous release number
    prevRelease = relNumber - 1
    if prevRelease < 0:
        prevRelease = 6

    #Loop over the result blocks
    for build in resultBlock:

        print( "\n> " + build.split( "/" )[0] )

        #Extract table block
        dateRow = resultBlock[ build ][ 0 ]
        testColumn = resultBlock[ build ][ 1 ]
        testCells = resultBlock[ build ][ 2 ]

        #Extract date/rel info for testing
        releaseString = dateRow[ relNumber ][ 0 ]
        dateString = dateRow[ relNumber ][ 1 ]
        localRelease = "rel_" + str( relNumber )
        localDateTime = dt.datetime.now()
        if yesterday:
            localDateTime -= dt.timedelta( days=1 )
        localDate = ""

        #Why bother with st/nd/rd/th logic? Just extract existing
        if "th" in dateString:
            localDate = localDateTime.strftime( "%dth %b" )

        elif "st" in dateString:
            localDate = localDateTime.strftime( "%dst %b" )

        elif "nd" in dateString:
            localDate = localDateTime.strftime( "%dnd %b" )

        elif "rd" in dateString:
            localDate = localDateTime.strftime( "%drd %b" )

        elif dateString == "???":
            localDate = "???"

        else:
            print( "Unrecognised date:" )
            print( dateString )
            exit()

        #Trim leading zero
        if localDate[0] == '0':
            localDate = localDate[1:]

        #Test date/rel info
        if not dateRow[ relNumber ] == [ localRelease, localDate ]:
            print( "Unexpected date/release info:" )
            print( "Expected " + localRelease + ", " + localDate )
            print( "Retrieved " + releaseString + ", " + dateString )

        elif dateString == "???":
            print( "Tests may not have run" )

        #Check the test results
        rowNumber = 0
        notOKcount = 0
        for cell in testCells[ relNumber ]:

            testName = testColumn[ rowNumber ]

            #If the tests are still running, examine yesterday
            if cell[ 0 ] == "running" and cell[ 1 ] == "n/a":
                prevCell = testCells[ prevRelease ][ rowNumber ]
                print( " - " + testName + " still running, previous result: " + formatRTTjob( prevCell, myScraper, testName ) )
                notOKcount += 1

            elif not ( cell[ 0 ] == "success" and cell[ 1 ] == "success" ):
                print( " - " + testName + ": " + formatRTTjob( cell, myScraper, testName ) )
                notOKcount += 1

            rowNumber += 1

        if notOKcount == 0:

            print( " - All OK" )


#Output the information from a single RTT job
def formatRTTjob( cell, myScraper, testName ):

    formattedJob = cell[ 0 ] + ", " + cell[ 1  ]

    #If an error was reported, examine the log files
    if cell[ 0 ] == "error" or cell[ 1 ] == "error":

        if "--nologs" in sys.argv:
            formattedJob += "\nDid not search logs"

        else:
            expectedLogName = testName + "_log"
            logResults = myScraper.scrapeRTTjob( cell[ 2 ], expectedLogName )
            formattedJob += "\n" + logResults[0] + ": " + logResults[1]

    return formattedJob
