import urllib.request
import os

# Made as a class so we can re-use webkit sessions
class ArtAnalysisTool():

    def __init__( self, scraper ):

        self.scraper = scraper
        self.summary = None
        self.followup = None
        self.paranoid = None

    # https://stackoverflow.com/questions/287871/how-do-i-print-colored-text-to-the-terminal
    def ColourText( self, r, g, b, text ):
        return f"\033[38;2;{r};{g};{b}m{text}\033[0m"
    def RedText( self, text ):
        return self.ColourText( 255, 0, 0, text )
    def GreenText( self, text ):
        return self.ColourText( 0, 255, 0, text )
    def YellowText( self, text ):
        return self.ColourText( 255, 255, 0, text )
    def FormatStatus( self, status, url ):
        if "failed" in status:
            # If test failed, find out why
            return self.RedText( status ) + ':\t' + self.scraper.ScrapeGridError( url )
        elif "finished" in status:
            return self.YellowText( status )
        elif "succeeded" in status:
            return self.GreenText( status )
        else:
            return status
    def FormatResults( self, resultsBlock ):
        status, url, results = resultsBlock
        resultString = ""
        for result in results:
            resultString += '\t' + result[0]
            if '0' == result[1]:
                resultString += ' ' + self.GreenText( result[1] )
            else:
                resultString += ' ' + self.YellowText( result[1] )
        if resultString == "":
            return self.FormatStatus( status, url )
        else:
            return self.FormatStatus( status, url ) + ":" + resultString


    #
    # Format a query to the ART page
    #
    def URLconstructor( self, packageList=None, branchList=None, nDays=7 ):

        defaultPackageList = [ "DigitizationTests", "DigitizationTestsMT", "ISF_Validation", "ISF_ValidationMT", "OverlayTests", "OverlayTestsMT", "SimCoreTests", "SimCoreTestsMT" ]
        defaultBranchList = [ "master/Athena/x86_64-centos7-gcc11-opt", "master/AthSimulation/x86_64-centos7-gcc11-opt", "22.0-mc20/Athena/x86_64-centos7-gcc8-opt", "22.0/Athena/x86_64-centos7-gcc11-opt", "22.0/AthSimulation/x86_64-centos7-gcc11-opt", "21.0/Athena/x86_64-centos7-gcc62-opt", "21.0/AthSimulation/x86_64-centos7-gcc62-opt" ]

        if packageList == None:
            packageList = defaultPackageList
        if branchList == None:
            branchList = defaultBranchList

        packageString = ','.join( packageList )
        branchString = ','.join( branchList )
        url = "https://bigpanda.cern.ch/art/tasks/?package=" + packageString + "&branch=" + branchString + "&nlastnightlies=" + str(nDays) + "&view=packages"
        return url


    #
    # Create the summary report, and cache the data for future use
    #
    def Summary( self ):

        if self.summary is None:
            defaultURL = self.URLconstructor()
            self.summary = self.scraper.ScrapeArtSummary( defaultURL )
        self.followup = []
        self.paranoid = []

        # Examine latest results, and look for change w.r.t. previous
        for testGroup in self.summary:
            print( "==============================" + testGroup + "==============================" )

            for build in self.summary[ testGroup ][ "results" ]:
                timeSeries = self.summary[ testGroup ][ "results" ][ build ]
                dateSeries = self.summary[ testGroup ][ "dates" ]
                linkSeries = self.summary[ testGroup ][ "links" ][ build ]

                retryCount = 0
                lastValid = -1
                prevValie = -1
                while retryCount < 3:

                    # Find last valid entry (need some results, need no active jobs)
                    lastValid = len( timeSeries ) - 1
                    while lastValid > 0 and ( len( timeSeries[ lastValid ] ) == 0 or timeSeries[ lastValid ][0] > 0 ):
                        lastValid -= 1

                    # Find penultimate valid entry (presumably no jobs still active but doesn't hurt to check)
                    prevValid = lastValid - 1
                    while prevValid >= 0 and ( len( timeSeries[ prevValid ] ) == 0 or timeSeries[ lastValid ][0] > 0 ):
                        prevValid -= 1
                
                    # If you need more results, do a targeted search for this test and build
                    if lastValid == 0 or prevValid < 0:
                        specificResults = self.scraper.ScrapeArtSummary( self.URLconstructor( [testGroup], [build], 3 + retryCount ) )
                        timeSeries = specificResults[ testGroup ][ "results" ][ build ]
                        dateSeries = specificResults[ testGroup ][ "dates" ]
                        linkSeries = specificResults[ testGroup ][ "links" ][ build ]
                        retryCount += 1
                    else:
                        break

                if lastValid < 0 or prevValid < 0:
                    print( "Unable to retrieve results" )
                    continue

                # Compare results
                lastGood = timeSeries[ lastValid ][1]
                prevGood = timeSeries[ prevValid ][1]
                goodDelta = lastGood - prevGood
                lastTotal = sum( timeSeries[ lastValid ] )
                prevTotal = sum( timeSeries[ prevValid ] )
                totalDelta = lastTotal - prevTotal

                # Find out when these jobs ran
                lastDate = dateSeries[ lastValid ]
                prevDate = dateSeries[ prevValid ]

                # Find links to the jobs
                lastLink = linkSeries[ lastValid ]
                prevLink = linkSeries[ prevValid ]

                doParanoid = True
                if totalDelta != 0:
                    print( build + " Change in total jobs from " + prevDate + " to " + lastDate + ": " + str( prevTotal ) + " to " + str( lastTotal ) )
                if goodDelta > totalDelta:
                    print( build + self.GreenText( " Improvement from " ) + prevDate + " to " + lastDate )
                    self.followup.append( ( testGroup, build, prevDate, prevLink, lastDate, lastLink ) )
                    self.CompareNightlyTest( prevDate, prevLink, lastDate, lastLink )
                    doParanoid = False
                if goodDelta < totalDelta:
                    print( build + self.RedText( " Worsening from " ) + prevDate + " to " + lastDate )
                    self.followup.append( ( testGroup, build, prevDate, prevLink, lastDate, lastLink ) )
                    self.CompareNightlyTest( prevDate, prevLink, lastDate, lastLink )
                    doParanoid = False

                # Regardless of change, make lists of failed jobs to follow up
                lastBad = timeSeries[ lastValid ][2] + timeSeries[ lastValid ][3]
                if lastBad > 0 and doParanoid:
                    print( build + self.YellowText( " Errors present, total unchanged from " ) + prevDate + " to " + lastDate )
                    self.paranoid.append( ( testGroup, build, prevDate, prevLink, lastDate, lastLink ) )
                    self.CompareNightlyTest( prevDate, prevLink, lastDate, lastLink )

    #
    # Compare results where cached summary indicates a change
    #
    def Followup( self ):

        if self.followup is None:
            print( "Please run Summary() first" )
            return

        print( "Following up " + str( len( self.followup ) ) + " changes" )
        self.CompareNightlies( self.followup )

    #
    # Compare results where cached summary indicates any error
    #
    def Paranoid( self ):

        if self.paranoid is None:
            print( "Please run Summary() first" )
            return

        print( "Paranoid follow up for " + str( len( self.paranoid ) ) + " items" )
        self.CompareNightlies( self.paranoid )

    #
    # Look at the individual results from each test in the nightlies
    #
    def CompareNightlies( self, resultSet ):

        for entry in resultSet:

            # Look at the detailed nightly results
            olderDate = entry[2]
            olderResults = self.scraper.ScrapeArtNightly( entry[3] )
            newerDate = entry[4]
            newerResults = self.scraper.ScrapeArtNightly( entry[5] )

            self.CompareNightlyTest( olderDate, olderResults, newerDate, newerResults )

    #
    # Specific followup for a nightly result
    #
    def CompareNightlyTest( self, olderDate, olderLink, newerDate, newerLink ):

        # Look up the nightly result page
        olderResults = self.scraper.ScrapeArtNightly( olderLink )
        newerResults = self.scraper.ScrapeArtNightly( newerLink )

        # Compare each test by name
        for testScript in newerResults:

            # Test may not have run before
            if testScript not in olderResults:
                print( testScript + " was run latest (" + newerDate + ") but not previously (" + olderDate + ")" )
                print( self.FormatResults( newerResults[ testScript ] ) )
            else:
                self.CompareJobs( testScript, olderDate, newerDate, olderResults[ testScript ], newerResults[ testScript ] )

        for testScript in olderResults:

            # Test may not have run this time
            if testScript not in newerResults:
                print( testScript + " was run previously (" + olderDate + ") but not latest (" + newerDate + ")" )
                print( self.FormatResults( olderResults[ testScript ] ) )

    #
    # Evaluate the difference in nightly results for a single test job
    #
    def CompareJobs( self, testScript, olderDate, newerDate, olderResults, newerResults ):

        testNewStatus, testNewURL, testNewResults = newerResults
        testOldStatus, testOldURL, testOldResults = olderResults

        # Compare results
        if testNewResults != testOldResults:
            print( "Comparing " + testScript + " results " + olderDate + " to " + newerDate + ":" )
            print( self.FormatResults( olderResults ) )
            print( self.FormatResults( newerResults ) )

            # Look for a new problem
            oldTotal = 0
            for oldResult in testOldResults:
                oldTotal += int( oldResult[1] )
            newTotal = 0
            for newResult in testNewResults:
                newTotal += int( newResult[1] )
            if newTotal > oldTotal:

                # Awkward string formatting just gives us a directory name
                self.CheckLogs( testOldURL, testScript[:-3] + "_" + "_".join( olderDate.split() ) )
                self.CheckLogs( testNewURL, testScript[:-3] + "_" + "_".join( newerDate.split() ) )

    #
    # Look at the logging output
    #
    def CheckLogs( self, url, directory ):

        filePage = self.scraper.ScrapeLogURL( url )
        fileList = self.scraper.ScrapeLogFiles( filePage )

        os.mkdir( directory )
        for file in fileList:
            urllib.request.urlretrieve( file[1], os.path.join( directory, file[0] ) )

        print( "Logs downloaded to " + directory )
