import dryscrape
from bs4 import BeautifulSoup
import time
from urllib.request import urlopen
import getpass

# Made as a class so we can re-use webkit sessions
class ArtScrapeTool():

    # Start the session
    def __init__( self ):
        self.session = dryscrape.Session()
        self.debug = False

    def LoginPrompt( self ):
        username = input( "Username: " )
        password = getpass.getpass()
        self.Login( username, password )

    def Login( self, username, password ):
        url = "https://bigpanda.cern.ch/oauth/login/cernoidc/"
        if self.debug:
            print( "Loading: " + url )
        
        # Get the webpage
        self.session.visit( url )
        
        # Waits for the page to load by looking for the login text box
        # Find the xpath by "inspect"-ing an element in the browser, then right click and copy xpath
        self.session.wait_for( lambda: self.session.at_xpath( '//*[@id="username"]' ), interval=0.5, timeout=10 )

        if self.debug:
            print( "Found login page" )

        # https://stackoverflow.com/questions/34852026/python-dryscrape-scrape-page-with-cookies
        usernameBox = self.session.at_xpath( '//*[@id="username"]' )
        usernameBox.set( username )
        passwordBox = self.session.at_xpath( '//*[@id="password"]' )
        passwordBox.set( password )

        # Push the button
        usernameBox.form().submit()

        # Wait for the default bigpanda dashboard
        if self.debug:
            print( "Waiting for login" )
        try:
            self.session.wait_for( lambda: self.session.at_xpath("/html/body/div[6]/ul/li[1]/div/div/div[1]/a/img"), interval=0.5, timeout=20 )
        except:
            print( "Login failed" )
            exit()
        else:
            print( "Login successful" )


    # Method for examining ART pages
    def ScrapeArtSummary( self, url ):

        if self.debug:
            print( "Loading ART summary page: " + url )

        # Get the webpage
        self.session.visit( url )
        
        # Waits for the page to load by looking for the main content table
        self.session.wait_for( lambda: self.session.at_xpath("/html/body/div[6]/div[1]/div/ul/li[1]/div/table/tbody/tr[2]/td[1]/a"), interval=0.5, timeout=10 )

        # Convert to soup
        response = self.session.body()
        soup = BeautifulSoup( response, "lxml" )

        # Find each group of tests requested
        testGroups = soup.find_all( "li", {"class":"accordion-art-item is-active"} )
        testGroupResults = {}
        for testGroup in testGroups:

            # The header of the test group
            testGroupName = testGroup.find( "a", {"class":"accordion-art-title"} ).get_text()

            # Examine the results table
            tableRows = testGroup.find_all( "tr" )

            # First row is the dates (and first cell is empty)
            allDates = []
            for tableHeader in tableRows[0].find_all( "th" )[1:]:
                allDates.append( tableHeader.get_text() )

            # Next rows are the results
            nightlyResults = {}
            nightlyLinks = {}
            for tableRow in tableRows[1:]:

                # First cell is the build name
                rowCells = tableRow.find_all( "td" )
                buildName = rowCells[0].get_text()
                nightlyResults[ buildName ] = []
                nightlyLinks[ buildName ] = []

                # Each following cell is the result for a nightly
                for rowCell in rowCells[1:]:

                    # Someimes nightlies did not run
                    if rowCell.find( "div", {"class":"nodata"} ):
                        nightlyResults[ buildName ].append( () )
                        nightlyLinks[ buildName ].append( "" )
                    else:
                        active = int( rowCell.find( "div", {"class":"active"} ).get_text() )
                        succeeded = int( rowCell.find( "div", {"class":"succeeded"} ).get_text() )
                        finished = int( rowCell.find( "div", {"class":"finished"} ).get_text() )
                        failed = int( rowCell.find( "div", {"class":"failed"} ).get_text() )
                        link = rowCell.find( "a" )[ "href" ]
                        link = "https://bigpanda.cern.ch" + link # restore the domain prefix
                        nightlyResults[ buildName ].append( ( active, succeeded, finished, failed ) )
                        nightlyLinks[ buildName ].append( link )

            testGroupResults[ testGroupName ] = { "dates":allDates, "results":nightlyResults, "links":nightlyLinks }

        if self.debug:
            print( "Analysed ART summary page" )
        return testGroupResults


    def ScrapeArtNightly( self, url ):

        if self.debug:
            print( "Loading ART nightly page: " + url )

        # Get the webpage
        self.session.visit( url )

        # Waits for the page to load by looking for the main content table
        self.session.wait_for( lambda: self.session.at_xpath("/html/body/div[6]/div[2]/div/ul/li/div/ul/li"), interval=0.5, timeout=10 )

        # Convert to soup
        response = self.session.body()
        soup = BeautifulSoup( response, "lxml" )

        # Get the table of tests
        testGroup = soup.find( "div", {"class":"accordion-art-nested-content"} )
        tests = testGroup.find_all( "tr" )

        # Create a result dictionary
        artResults = {}

        # First two rows are formatting, examine the rest
        for test in tests[2:]:
            testScript = test.find( "td", {"class":"cell-left"} ).find( "a" ).get_text()

            # Get overall test job outcome
            testSummary = test.find( "div", {"class":"clickable"} )
            testURL = testSummary.find( "a" )[ "href" ]
            testURL = "https://bigpanda.cern.ch" + testURL # restore the domain prefix
            overallStatus = testSummary.find( "div" ).get_text().strip(' ')

            # Get sub-test results
            testResultsBlob = test.find( "td", {"class":"cell"} )
            subtests = testResultsBlob.find_all( "li" )

            # Look for subtest box ToolTips, which give the status as well
            artResults[ testScript ] = ( overallStatus, testURL, [] )
            for subtest in subtests:
                subtestTooltip = subtest.find( "span" ).get_text().strip(' ')
                subtestName, subtestResult = subtestTooltip.split(':')
                artResults[ testScript ][2].append( ( subtestName, subtestResult ) )

        return artResults


    def ScrapeGridError( self, url ):

        if self.debug:
            print( "Loading grid result page: " + url )

        # Get the webpage
        self.session.visit( url )

        # Waits for the page to load by looking for the job parameters table
        self.session.wait_for( lambda: self.session.at_xpath("/html/body/div[6]/table[1]/thead/tr/th"), interval=0.5, timeout=10 )

        # Convert to soup
        response = self.session.body()
        soup = BeautifulSoup( response, "lxml" )

        # Get the grid error message
        errorTable = soup.find( "table", {"class":"fresh-table unstriped alert"} )
        errorMessage = errorTable.find( "tbody" ).find( "tr" ).find( "td" ).get_text()
        return errorMessage
        
    def ScrapeLogURL( self, url ):

        if self.debug:
            print( "Loading grid result page: " + url )

        # Get the webpage
        self.session.visit( url )

        # Waits for the page to load by looking for the job parameters table
        self.session.wait_for( lambda: self.session.at_xpath("/html/body/div[6]/table[1]/thead/tr/th"), interval=0.5, timeout=10 )

        # Convert to soup
        response = self.session.body()
        soup = BeautifulSoup( response, "lxml" )

        # Get the link to the log files from the dropdown menu
        allDropdowns = soup.find_all( "div", {"class":"dropdown-items"} )
        for button in allDropdowns:

            allOptions = button.find_all( "a" )
            for option in allOptions:

                if "Log files" in option.get_text():

                    return "https://bigpanda.cern.ch" + option[ "href" ] # restore domain prefix

    def ScrapeLogFiles( self, url ):

        if self.debug:
            print( "Loading log file page: " + url )

        # Get the webpage
        self.session.visit( url )

        # Waits for the page to load by looking for the file list table
        self.session.wait_for( lambda: self.session.at_xpath('//*[@id="filelist"]'), interval=0.5, timeout=10 )

        # Convert to soup
        response = self.session.body()
        soup = BeautifulSoup( response, "lxml" )

        # Examine file list table
        results = []
        fileList = soup.find( "table", {"id":"filelist"} ).find_all( "tr" )
        for file in fileList:

            fileInfo = file.find( "a" )
            fileName = fileInfo.get_text().strip(' ').strip()
            fileURL = fileInfo[ "href" ]

            # Interesting log files only
            if "log." == fileName[:4] or ".log" == fileName[-4:]:

                results.append( ( fileName, fileURL ) )

        return results
