import time
import datetime
import sys
import re
from scrapeTool import *
from formatTool import *

#Process command line args
yesterday = ( "--yesterday" in sys.argv )

#Get the relevant timestamp
todayTime = datetime.datetime.now()
if yesterday:
    todayTime -= datetime.timedelta( days = 1 )

#Get the release number
relNumber = todayTime.weekday() + 1
if relNumber == 7:
    relNumber = 0
release = "rel_" + str( relNumber )

#Format the date
yesterdayTime = todayTime - datetime.timedelta( days = 1 )
timeString = todayTime.strftime( "%Y-%m-%d" )
yesterdayString = yesterdayTime.strftime( "%Y-%m-%d" )

#Configure report structure
#builds = [ "20.7.X-VAL", "20.7.X", "20.3.X.Y-VAL-Prod", "19.2.X.Y-VAL-Prod" ]
builds = [ "20.7.X-VAL", "19.2.X.Y-VAL-Prod" ]
buildGcc = { "20.7.X-VAL": "gcc49" }
#buildGcc[ "20.7.X" ] = "gcc49"
#buildGcc[ "20.3.X.Y-VAL-Prod" ] = "gcc48"
buildGcc[ "19.2.X.Y-VAL-Prod" ] = "gcc47"
buildProjects = { "20.7.X-VAL": [ "AtlasSimulation", "AtlasOffline" ] }
#buildProjects[ "20.7.X" ] = [ "AtlasSimulation", "AtlasOffline" ]
#buildProjects[ "20.3.X.Y-VAL-Prod" ] = [ "AtlasProduction" ]
buildProjects[ "19.2.X.Y-VAL-Prod" ] = [ "AtlasProduction" ]
projectContainers = { "AtlasSimulation": [] }
projectContainers[ "AtlasOffline" ] = [ "Event/EventOverlay", "Generators", "Simulation" ]
projectContainers[ "AtlasProduction" ] = []
projectContainers[ "Athena" ] = []

print( "=====================================================" )
print( "Simulation/digitization shift report: NICOS/ATN " + release )
print( "=====================================================" )

#Loop over all builds
myScraper = ScrapeTool()

#Make a bunch of regexps for packages to check
#Defined here (TODO: pull in automatically)
#https://gitlab.cern.ch/atlas/athena/blob/master/CI/domain_map.py
newBuildProjects = [ "Athena" ]
packageFilterRules = [ 'Digitization', 'TileSimAlgs', 'PileUpComps', 'PileUpTools', 'RunDependentSim', 'Overlay', '^Simulation/', 'G4', '^Generators/' ]
packageFilters = []
for rule in packageFilterRules:
    packageFilters.append( re.compile( rule ) )

#21
print( "\n********************************* 21.0 Athena " + timeString + " ****************************************" )
print( "\n____ NICOS ____:" )
#I'm not sure why the builds seem to be a day behind, but hey ho
url = "http://atlas-nightlies-browser.cern.ch/~platinum/nightlies/info?tp=c&nightly=21.0_Athena_x86_64-slc6-gcc62-opt&rel=" + yesterdayString + "T*&ar=x86_64-slc6-gcc62-opt&proj=*"
resultBlock = myScraper.ScrapePlatinum( url, newBuildProjects, projectContainers, packageFilters, False )
FormatPlatinum( resultBlock, newBuildProjects )

print( "\n_____ ATN _____:" )
#ATN dates are up-to-date
url = "http://atlas-nightlies-browser.cern.ch/~platinum/nightlies/info?tp=t&nightly=21.0_ATN_x86_64-slc6-gcc62-opt&rel=" + timeString + "T*&ar=x86_64-slc6-gcc62-opt&proj=*"
resultBlock = myScraper.ScrapePlatinum( url, newBuildProjects, projectContainers, packageFilters, True )
FormatPlatinum( resultBlock, newBuildProjects )

print( "\n********************************* 21.3 Athena " + timeString + " ****************************************" )
print( "\n____ NICOS ____:" )
url = "http://atlas-nightlies-browser.cern.ch/~platinum/nightlies/info?tp=c&nightly=21.3_Athena_x86_64-slc6-gcc62-opt&rel=" + yesterdayString + "T*&ar=x86_64-slc6-gcc62-opt&proj=*"
resultBlock = myScraper.ScrapePlatinum( url, newBuildProjects, projectContainers, packageFilters, False )
FormatPlatinum( resultBlock, newBuildProjects )

print( "\n_____ ATN _____:" )
url = "http://atlas-nightlies-browser.cern.ch/~platinum/nightlies/info?tp=t&nightly=21.3_ATN_x86_64-slc6-gcc62-opt&rel=" + timeString + "T*&ar=x86_64-slc6-gcc62-opt&proj=*"
resultBlock = myScraper.ScrapePlatinum( url, newBuildProjects, projectContainers, packageFilters, True )
FormatPlatinum( resultBlock, newBuildProjects )

print( "\n********************************* 21.0 AthSimulation " + timeString + " ****************************************" )
print( "\n____ NICOS ____:" )
url = "http://atlas-nightlies-browser.cern.ch/~platinum/nightlies/info?tp=c&nightly=21.0_AthSimulation_x86_64-slc6-gcc62-opt&rel=" + yesterdayString + "T*&ar=x86_64-slc6-gcc62-opt&proj=*"
resultBlock = myScraper.ScrapePlatinum( url, newBuildProjects, projectContainers, packageFilters, False )
FormatPlatinum( resultBlock, newBuildProjects )


#Master on Friday (not when we want a retrospective comparison)
if release == "rel_5" and not yesterday:
    print( "\n********************************* master Athena " + timeString + " ****************************************" )
    print( "\n____ NICOS ____:" )
    url = "http://atlas-nightlies-browser.cern.ch/~platinum/nightlies/info?tp=c&nightly=master_Athena_x86_64-slc6-gcc62-opt&rel=" + yesterdayString + "T*&ar=x86_64-slc6-gcc62-opt&proj=*"
    resultBlock = myScraper.ScrapePlatinum( url, newBuildProjects, projectContainers, packageFilters, False )
    FormatPlatinum( resultBlock, newBuildProjects )

    print( "\n_____ ATN _____:" )
    url = "http://atlas-nightlies-browser.cern.ch/~platinum/nightlies/info?tp=t&nightly=master_ATN_x86_64-slc6-gcc62-opt&rel=" + timeString + "T*&ar=x86_64-slc6-gcc62-opt&proj=*"
    resultBlock = myScraper.ScrapePlatinum( url, newBuildProjects, projectContainers, packageFilters, True )
    FormatPlatinum( resultBlock, newBuildProjects )

    print( "\n********************************* master AthSimulation " + timeString + " ****************************************" )
    print( "\n____ NICOS ____:" )
    url = "http://atlas-nightlies-browser.cern.ch/~platinum/nightlies/info?tp=c&nightly=master_AthSimulation_x86_64-slc6-gcc62-opt&rel=" + yesterdayString + "T*&ar=x86_64-slc6-gcc62-opt&proj=*"
    resultBlock = myScraper.ScrapePlatinum( url, newBuildProjects, projectContainers, packageFilters, False )
    FormatPlatinum( resultBlock, newBuildProjects )

for build in builds:

    print( "\n************************************** " + build + " *************************************************" )

    #Compilation
    print( "\n____ NICOS ____:" )
    url = "http://atlas-nightlies-browser.cern.ch/~platinum/nightlies//info?tp=c&nightly=" + build + "&rel=" + release + "&ar=x86_64-slc6-" + buildGcc[ build ] + "-opt&proj=*"
    resultBlock = myScraper.ScrapePlatinum( url, buildProjects[build], projectContainers, [], False )
    FormatPlatinum( resultBlock, buildProjects[build] )

    #ATN
    print( "\n_____ ATN _____:" )
    url = "http://atlas-nightlies-browser.cern.ch/~platinum/nightlies//info?tp=t&nightly=" + build + "&rel=" + release + "&ar=x86_64-slc6-" + buildGcc[ build ] + "-opt&proj=*"
    resultBlock = myScraper.ScrapePlatinum( url, buildProjects[build], projectContainers, [], True )
    FormatPlatinum( resultBlock, buildProjects[build] )
