# nicoScrape

Use [DryScrape](https://github.com/niklasb/dryscrape) and [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/) in python to scrape the NICOS pages

[Shift instructions](https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SimulationDigitizationNightlyValidation#3_Simulation_RTT)

[Scraping approach based on this example](http://stackoverflow.com/questions/8049520/web-scraping-javascript-page-with-python)

Obviously cannot replace editorial comments in the ELOG, but will do all the tedious web-crawling for you.

## Ubuntu setup:
```
sudo apt-get install qt5-default libqt5webkit5-dev build-essential python-lxml python-pip xvfb python-bs4
sudo pip3 install dryscrape
git clone https://gitlab.cern.ch/atlas-simulation-team/nicoScrape
```

## Usage instructions:
```
#Morning report
python3 nicoScrape.py

#Afternoon report
python3 rttScrape.py
```

Logs are intended to be diff-able, so I recommend you compare each output to the day before.
Command-line argument included for convenience:
```
#Suggested usage
python3 nicoScrape.py | tee today.log
python3 nicoScrape.py --yesterday | tee yesterday.log
vimdiff today.log yesterday.log
```

The RTT output log files can now be analysed, but finding the relevant snippet in the relevant log can take a long time.
In addition, inspecting the log files might cause crashes due to authentication problems. 
You can skip log inspection like this:
```
python3 rttScrape.py --nologs
```

## Caveat emptor

Makes no attempt to analyse Coverity data (requires web login - is there an API?) or Profiling/PMB plots (plots are rasterised).
