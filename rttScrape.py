import time
import sys
from scrapeTool import *
from formatTool import *

#Process command line args
yesterday = ( "--yesterday" in sys.argv )

#Get the release number
relNumber = time.localtime().tm_wday
if not yesterday:
    relNumber += 1
if relNumber == 7:
    relNumber = 0
release = "rel_" + str( relNumber )

#Make the list of URLs to check
#Compare to twiki page: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SimulationDigitizationNightlyValidation#3_Simulation_RTT
urlList = {}
urlList[ "ISF G4" ] = []
urlList[ "ISF G4" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=ISF_Validation;jobname=G4_*;branch=master;project=Athena;verbosity=vvv;%29" ]
urlList[ "ISF G4" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=ISF_Validation;jobname=G4_*;branch=21.3;project=Athena;verbosity=vvv;%29" ]
urlList[ "ISF G4" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=ISF_Validation;jobname=G4_*;branch=21.0;project=Athena;verbosity=vvv;%29" ]
urlList[ "ISF G4" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=ISF_Validation;jobname=G4_*;branch=19.2.X.Y-VAL;project=AtlasProduction;verbosity=vvv;%29" ]
#
urlList[ "ISF ATLFASTII" ] = []
urlList[ "ISF ATLFASTII" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=ISF_Validation;jobname=ATLFASTII_*;branch=master;project=Athena;verbosity=vvv;%29" ]    
urlList[ "ISF ATLFASTII" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=ISF_Validation;jobname=ATLFASTII_*;branch=21.3;project=Athena;verbosity=vvv;%29" ]
urlList[ "ISF ATLFASTII" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=ISF_Validation;jobname=ATLFASTII_*;branch=21.0;project=Athena;verbosity=vvv;%29" ]
urlList[ "ISF ATLFASTII" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=ISF_Validation;jobname=ATLFASTII_*;branch=19.2.X.Y-VAL;project=AtlasProduction;verbosity=vvv;%29" ]
#
urlList[ "ISF ATLFASTIIF" ] = []
urlList[ "ISF ATLFASTIIF" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=ISF_Validation;jobname=*ATLFASTIIF_*;branch=master;project=Athena;verbosity=vvv;%29" ]
urlList[ "ISF ATLFASTIIF" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=ISF_Validation;jobname=*ATLFASTIIF_*;branch=21.3;project=Athena;verbosity=vvv;%29" ]
urlList[ "ISF ATLFASTIIF" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=ISF_Validation;jobname=*ATLFASTIIF_*;branch=21.0;project=Athena;verbosity=vvv;%29" ]
#
urlList[ "SimCoreTests RTT (AtlasG4)" ] = []
urlList[ "SimCoreTests RTT (AtlasG4)" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=SimCoreTests;branch=19.2.X.Y-VAL;cmtconfig=x86_64-slc6-gcc47-opt;project=AtlasProduction;verbosity=vvv;%29" ]
urlList[ "SimCoreTests RTT (AtlasG4)" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=SimCoreTests;branch=21.3;project=Athena;verbosity=vvv;%29" ]
urlList[ "SimCoreTests RTT (AtlasG4)" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=SimCoreTests;branch=21.0;project=Athena;verbosity=vvv;%29" ]
urlList[ "SimCoreTests RTT (AtlasG4)" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=SimCoreTests;branch=master;project=Athena;verbosity=vvv;%29" ]
#
urlList[ "Evgen and Simulation for SUSY and Exotics physics" ] = []
urlList[ "Evgen and Simulation for SUSY and Exotics physics" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=SimExoticsTests;branch=19.2.X.Y-VAL;project=AtlasProduction;verbosity=vvv;viewby=packagename%29" ]
urlList[ "Evgen and Simulation for SUSY and Exotics physics" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=SimExoticsTests;branch=21.0;project=Athena;verbosity=vvv;viewby=packagename%29" ]
urlList[ "Evgen and Simulation for SUSY and Exotics physics" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=SimExoticsTests;branch=master;project=Athena;verbosity=vvv;viewby=packagename%29" ]
#
urlList[ "Frozen Showers" ] = [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=LArG4Validation;branch=dev;project=AtlasProduction;verbosity=vvv;viewby=packagename%29" ]
#
urlList[ "Core Digitization" ] = []
urlList[ "Core Digitization" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28jobname=DigiPerformanceMu*,mc15*;packagename=DigitizationTests;branch=master;project=Athena;verbosity=vvv;viewby=packagename%29" ]
urlList[ "Core Digitization" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28jobname=DigiPerformanceMu*,mc15*;packagename=DigitizationTests;branch=21.0;project=Athena;verbosity=vvv;viewby=packagename%29" ]
urlList[ "Core Digitization" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28jobname=DigiPerformanceMu*,mc15*;packagename=DigitizationTests;branch=20.7.*;project=AtlasProduction;verbosity=vvv;viewby=packagename%29" ]
#
urlList[ "Overlay" ] = [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=OverlayMonitoringRTT;branch=20.7*;project=AtlasProduction;verbosity=vvv;%29" ]
urlList[ "Overlay" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=OverlayMonitoringRTT;branch=21.0;project=Athena;verbosity=vvv;%29" ]
urlList[ "Overlay" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=OverlayMonitoringRTT;branch=master;project=Athena;verbosity=vvv;%29" ]
#
urlList[ "Digitization TCT job" ] = []
urlList[ "Digitization TCT job" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=Tier0ChainTests;branch=20.7*;project=AtlasProduction;jobname=DigitizationTest;verbosity=vvv%29" ]
urlList[ "Digitization TCT job" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=Tier0ChainTests;branch=21.0*,master;project=Athena;jobname=DigitizationTest;verbosity=vvv%29" ]
#
urlList[ "DigiMReco_trf job in RecJobTransformTests" ] = []
urlList[ "DigiMReco_trf job in RecJobTransformTests" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28project=AtlasProduction;packagename=RecJobTransformTests;jobname=DigiMReco_trf_FCT,DigiMReco_tf_FCT;verbosity=vvv;viewby=packagename%29" ]
urlList[ "DigiMReco_trf job in RecJobTransformTests" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=RecJobTransformTests;branch=master,21.0;project=Athena;jobname=DigiMReco_tf_FCT;verbosity=vvv;viewby=packagename%29" ]
#
urlList[ "Full Chain Tests" ] = []
urlList[ "Full Chain Tests" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=FullChainTests;branch=master;project=Athena;verbosity=vvv;%29" ]
urlList[ "Full Chain Tests" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=FullChainTests;branch=21.0;project=Athena;verbosity=vvv;%29" ]
urlList[ "Full Chain Tests" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=FullChainTests;branch=20.7*;project=AtlasProduction;verbosity=vvv;%29" ]
urlList[ "Full Chain Tests" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=FullChainTests;branch=19.2.X.Y-VAL;project=AtlasProduction;verbosity=vvv;%29" ]
#
urlList[ "Coverity" ] = []
#
urlList[ "Profiling Jobs" ] = [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=SimPerformanceTests;project=AtlasProduction;verbosity=vvv;viewby=packagename%29" ]
urlList[ "Profiling Jobs" ] += [ "https://atlas-rtt.cern.ch/index.php?q=%28packagename=DigitizationTests;jobname=DigiPerformanceMu*;branch=*;verbosity=vvv;%29" ]
#
orderedTests = [ "ISF G4", "ISF ATLFASTII", "ISF ATLFASTIIF", "SimCoreTests RTT (AtlasG4)", "Evgen and Simulation for SUSY and Exotics physics", "Frozen Showers", "Core Digitization", "Overlay", "Digitization TCT job", "DigiMReco_trf job in RecJobTransformTests", "Full Chain Tests", "Coverity", "Profiling Jobs" ]

#Extra stuff on Wednesday
#if release == "rel_3":
    #print( "DO MORE THINGS TODAY" )
    #exit()

print( "==============================================================" )
print( "Simulation/digitization shift report: RTT/FCT/TCT/PMB, " + release )
print( "==============================================================" )

#Examine all URLs
myScraper = ScrapeTool()
for testGroup in orderedTests:

    print( "\n---------------------------" )
    print( "*** " + testGroup + " ***" )
    print( "---------------------------" )

    for url in urlList[ testGroup ]:

        #Retrieve the RTT results
        title, results = myScraper.ScrapeRTT( url )
        
        if (title is "no_matches"):
            # empty rtt page
            # get release from url name
            thisrel=[br for br in url.split(";") if "branch" in br]
            print ("> ",thisrel[0].split("=")[1])
            print ("   no matches")
            
        else:
            FormatRTT( results, relNumber, yesterday, myScraper )

        #exit() #for debugging
