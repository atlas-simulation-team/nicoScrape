# Name environment
ENV_NAME=artistEnv
ACTIVATE=${PWD}/${ENV_NAME}/bin/activate

#sudo apt install libqt5webkit5-dev

# Install packages if this is fresh start
if [[ ! -e "${ACTIVATE}" ]]; then
    python3 -m virtualenv ${ENV_NAME}
    source ${ACTIVATE}
    pip3 install dryscrape beautifulsoup4

else
    source ${ACTIVATE}
fi

