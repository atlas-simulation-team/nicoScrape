import dryscrape
from bs4 import BeautifulSoup
import time
from urllib.request import urlopen

#Made as a class so we can re-use webkit sessions
class ScrapeTool():

    #Start the session
    def __init__( self ):
        self.session = dryscrape.Session()

    #Method for examining NICOS pages
    def ScrapePlatinum( self, url, buildProjects, projectContainers, packageFilter, isATN ):

        print( url )
        #Make the return type
        returnBlock = {}
        for project in buildProjects:
            returnBlock[ project ] = []

        #Get the webpage and turn it into soup
        self.session.visit( url )
        time.sleep( 5 ) #Page takes a while to query backend
        response = self.session.body()
        soup = BeautifulSoup( response, "lxml" )

        #Find the main data table
        dataTable = soup.find( role="alert" )
        if dataTable is None:
            print( "No data found for URL" )
            print( url )
            exit()

        #Examine each row
        for row in dataTable.children:

            #Check for bad query
            if row.contents[2].contents[0] == "Query did not provide any result":
                print( "\nTest did not run" )
                return returnBlock

            #Pull out data
            state = row.contents[0].contents[0].contents[1]["title"]        #row -> 1st column -> div -> span (title attribute)
            project = row.contents[1].string                                #row -> 2nd column (text)
            logfile = row.contents[2].contents[0]["href"]                   #row -> 3rd column -> div (link attribute)
            package = row.contents[2].contents[0].string                    #row -> 3rd column -> div (text)
            container = row.contents[4].string
            if isATN:
                container = row.contents[5].string
            #package = package.split( '#' )[0]                              #Might be relevant to ATN

            #Report on relevant issues
            if state != "OK" and project in buildProjects:

                #Restrict the containers in some cases (empty list is wildcard)
                containers = projectContainers[ project ]
                if len( containers ) == 0 or container in containers:

                    matchedPackage = ( len( packageFilter ) == 0 )
                    for packageRegexp in packageFilter:
                        if packageRegexp.search( package ) or packageRegexp.search( container ):
                            matchedPackage = True
                            break

                    #Restrict packages the same way
                    if matchedPackage:

                        #Pull out the error message
                        self.session.visit( logfile )
                        logresponse = self.session.body()
                        logsoup = BeautifulSoup( logresponse, "lxml" )
                        problemLine = logsoup.find( href="#prblm" )
                        if problemLine is None:
                            returnBlock[ project ] += [ ( package, state, "" ) ]
                        else:
                            message = logsoup.find( href="#prblm" ).string

                            if message is None:
                                #Catches an issue where html strings get extra formatting if they contain &
                                message = logsoup.find( href="#prblm" ).text

                            #Make a tuple of the relevant info
                            returnBlock[ project ] += [ ( package, state, message ) ]

        return returnBlock

    #Method for examining RTT pages
    def ScrapeRTT( self, url ):

        #Get the page
        self.session.visit( url )
        response = self.session.body()
        soup = BeautifulSoup( response, "lxml" )

        #Find the results table
        dataTables = soup.find_all( "table" )
        resultsTables = []
        for table in dataTables:

            #Note class can be multi-valued, so stored as array
            if table.has_attr( "class" ) and table[ "class" ] == ["results"]:

                resultsTables += [ table ]

        #Check that something was found
        if not len( resultsTables ) == 1:

            print( "Found " + str( len( resultsTables ) ) + " results tables (expected 1)" )
            exit()

        table = resultsTables[0]
        dateRow = []
        testColumn = []
        testGroup = ""
        build = ""
        results = [ [], [], [], [], [], [], [] ] #7 days, 7 releases
        resultBlock = {}

        #Go down 1 layer for the tbody tag
        for row in table.contents[0].children:

            #At the top of each build block is the date information, which is formatted differently
            if row.has_attr( "class" ) and row[ "class" ] == [ "st_colheaders_row" ]:

                #Get all the columns in the row (find_all to filter some crap)
                for date in row.find_all( "td" ):

                    #The first cell is empty
                    if date.has_attr( "class" ):

                        if not date[ "class" ] == [ "rowheader_cell" ]:

                            print( "Format error" )
                            exit()

                    #Get the date/release information
                    else:
                        #It's got a </br> in it, so annoyingly we have to squish into .text and then unpack
                        releaseString = date.text[0:5]
                        dateString = date.text[5:]
                        dateRow.append( [ releaseString, dateString ] )

            #All other rows should be headers, subheaders, or results
            else:
                #Get all the columns in the row (find_all to filter some crap)
                columnCount = 0
                for column in row.find_all( "td" ):

                    #Columns have classes, remember class can be multivalued so is array
                    if not column.has_attr( "class" ):

                        print( "Unrecognised column:" )
                        print( column )
                        exit()

                    #The title for the row - test name
                    elif column[ "class" ] == [ "rowheader_cell" ]:

                        testColumn.append( column.string )

                    #Table header indicates the test group
                    elif column[ "class" ] == [ "header" ]:

                        testGroup = column.string

                    #Table subheader is build info, and indicates a new block
                    elif column[ "class" ] == [ "subheader" ]:

                        #Store old block (skip empty)
                        if not build == "":
                            resultBlock[ build ] = ( dateRow, testColumn, results )

                        #Start new block
                        build = column.string
                        dateRow = []
                        testColumn = []
                        results = [ [], [], [], [], [], [], [] ]

                    #Result cell
                    elif column[ "class" ] == [ "cell" ]:

                        #Enter the cell
                        cell = column.contents[0].contents[0]

                        #Sometimes the tests don't run
                        if cell == "n/a":
                            results[ columnCount ].append( [ "n/a", "n/a" ] )
                        else:
                            #Retrieve the results, and a link to the job page
                            joburl = "https://atlas-rtt.cern.ch/index.php" + cell[ "href" ]
                            results[ columnCount ].append( [ cell.contents[0].string, cell.contents[1].string, joburl ] )

                        #Update position in the row
                        columnCount += 1
                        
                    elif column[ "class" ] == [ "no_matches" ]:
                        # empty rtt page
                        testGroup = "no_matches"
                        
                    else:
                        print( "Unrecognised column:" )
                        print( column )
                        exit()

        #Store old block (skip empty)
        if not build == "":
            resultBlock[ build ] = ( dateRow, testColumn, results )

        #Return result
        return testGroup, resultBlock

    #Visit the job page for an RTT and examine logs
    def scrapeRTTjob( self, joburl, expectedLogName ):

        #Visit the job page
        self.session.visit( joburl )
        jobresponse = self.session.body()
        jobsoup = BeautifulSoup( jobresponse, "lxml" )

        #Find the job output files
        fileList = jobsoup.find( "li" )
        if fileList is None:
            print( "Format error on job page: could not find file list" )
        elif not len( fileList.contents ) == 2:
            print( "Format error on job page: file list format" )
        else:
            outputFileInfo = fileList.contents[1].find_all( "li" )
            relevantLogs = []
            expectedLog = ""
            postprocessingLogs = []
            for fileInfo in outputFileInfo:

                #Skip tree branches - only want leaves
                if not fileInfo.has_attr( "class" ):

                    fileName = fileInfo.contents[0].string
                    if len( fileName ) >= 3 and fileName[0:3] == "log":
                        relevantLogs.append( [ fileName, "https://atlas-rtt.cern.ch/" + fileInfo.contents[0]["href"] ] )

                    elif fileName == expectedLogName:
                        expectedLog = "https://atlas-rtt.cern.ch/" + fileInfo.contents[0]["href"]

                    elif len( fileName ) >= 18 and fileName[-18:] == "postprocessing.log":
                        postprocessingLogs.append( [ fileName, "https://atlas-rtt.cern.ch/" + fileInfo.contents[0]["href"] ] )


            #Check the main log for the job after the individual components
            if not expectedLog == "":
                relevantLogs.append( [ expectedLogName, expectedLog ] )

            #Read the logs
            for logurl in relevantLogs:

                #We don't need anything fancy to read a text file
                logresponse = urlopen( logurl[1] )

                #Just return the first important message
                for lineBytes in logresponse:

                    #For some reason the file needs explicit decoding now. Brute force error handling
                    try:
                        line = lineBytes.decode( "utf-8" )
                    except:
                        line = ""

                    #Look for problems
                    if "FATAL" in line or "ERROR" in line:
                        return logurl[0], line[:-1] #trim newline

            #Finally check postprocessing
            for logurl in postprocessingLogs:

                #We don't need anything fancy to read a text file
                logresponse = urlopen( logurl[1] ).read()

                #Just return the first important message - these are python errors not athena
                for lineBytes in logresponse:

                    #For some reason the file needs explicit decoding now. Brute force error handling
                    try:
                        line = lineBytes.decode( "utf-8" )
                    except:
                        line = ""

                    #Look for problems
                    if "Fatal:" in line or "Error:" in line or "Warning:" in line:
                        return logurl[0], line[:-1] #trim newline

            #If you haven't returned something by now, there's a problem
            return "Did not find relevant log entry", ""
